## Local ORY Kratos Setup

Project to be able to set ORY Kratos up for local interactions.

To spin up the local instance:
```
docker-compose up --build --force-recreate
```

Interact with the UI at http://localhost:4455/welcome

To clean up the local instance:
```
docker-compose down -v
```
